<?php

namespace App\Http\Controllers;

use App\Http\Resources\PostResource;
use App\Post;
use Response;

class PostsController extends Controller
{
    public function show(Post $post)
    {
//        return new PostResource($post);

        if($post->isPublished() || auth()->check())
        {
            $post->load('owner', 'category', 'tags', 'photos');

            if(request()->wantsJson())
                return response::json([
                    'post' => $post
                ]);

            return view('posts.show', compact('post'));
        }


        abort(404);
    }
}
