<?php

namespace App\Http\Controllers;

use App\Tag;
use Illuminate\Http\Request;
use Response;

class TagsController extends Controller
{
    public function show(Tag $tag)
    {
        $posts = $tag->posts()->published()->paginate();

        if (request()->wantsJson())
            return response::json([
                'posts' => $posts
            ]);

        return view('pages.home', [
            'title' => "Publicaciones de la etiqueta '{$tag->name}'",
            'posts' => $posts
        ]);
    }
}
