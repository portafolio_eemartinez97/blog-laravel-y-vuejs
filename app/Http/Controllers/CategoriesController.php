<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;
use Response;

class CategoriesController extends Controller
{
    public function show(Category $category)
    {
        $posts = $category->posts()->published()->paginate();

        if (request()->wantsJson())
            return response::json([
                'posts' => $posts
            ]);

        return view('pages.home', [
            'title' => "Publicaciones de la categoria '{$category->name}'",
            'posts' => $posts
        ]);
    }
}
