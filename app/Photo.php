<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Photo extends Model
{
    protected $guarded = [];

    protected static function boot()
    {
        parent::boot();

        static::deleting(function ($photo) {
            $photoPath = str_replace('/storage/', '', $photo->path);
            Storage::disk('public')->delete($photoPath);
        });
    }

    public function getPathAttribute($path) {
        return '/storage/' . $path;
    }
}
