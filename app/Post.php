<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Post extends Model
{
    protected $fillable = ['title', 'body', 'iframe', 'excerpt', 'published_at', 'category_id', 'user_id'];

    protected $dates = ['published_at'];

    protected $appends = ['published_date'];

    protected static function boot()
    {
        parent::boot();

        static::deleting(function($post) {
            $post->tags()->detach();
            $post->photos->each->delete();
        });
    }

    public function getPublishedDateAttribute()
    {
        return optional($this->published_at)->format('M d');
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function tags()
    {
        return $this->belongsToMany(Tag::class);
    }

    public function scopePublished($query)
    {
        $query->with(['owner', 'category', 'tags', 'photos'])
            ->whereNotNull('published_at')
            ->where('published_at', '<=', Carbon::now())
            ->latest('published_at');
    }

    public function scopeAllowed($query)
    {
        if( auth()->user()->can('view', $this))
            return $query;

        return $query->where('user_id', auth()->id());
    }

    public function scopeByYearAndMonth($query)
    {
        return $query->selectRaw('year(published_at) as year')
            ->selectRaw('month(published_at) as month')
            ->selectRaw('monthName(published_at) as monthname')
            ->selectRaw('count(*) as posts')
            ->groupBy('year', 'month', 'monthname')
            ->orderBy('published_at');
    }


    public static function create(array $attributes = [])
    {
        $attributes['user_id'] = auth()->id();

        $post = static::query()->create($attributes);

        $post->generarUrl();

        return $post;
    }

    public function generarUrl()
    {
        $url = str_slug($this->title);

        if( $this::whereUrl($url)->exists())
        {
            $url = "{$url}-{$this->id}";
        }

        $this->url = $url;

        $this->save();
    }

    public function photos()
    {
        return $this->hasMany(Photo::class);
    }

    public function owner()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function isPublished()
    {
        return !is_null($this->published_at) && $this->published_at < today();
    }

    public function getRouteKeyName()
    {
        return 'url';
    }

    public function setPublishedAtAttribute($published_at)
    {
        $this->attributes['published_at'] = $published_at ? Carbon::parse($published_at) : null;
    }

    public function setCategoryIdAttribute($category)
    {
        $this->attributes['category_id'] = Category::find($category)
            ? $category
            : Category::create(["name" => $category])->id;
    }

    public function syncTags($tags)
    {
        $tagsIds = collect($tags)->map(function($tag) {
            return Tag::find($tag) ? $tag : Tag::create(["name" => $tag])->id;
        });

        return $this->tags()->sync($tagsIds);
    }

    public function viewType($home = '')
    {
        $view = null;

        if($this->photos->count() === 1):
            $view = 'posts.photo';
        elseif ($this->photos->count() > 1):
            $view = $home === 'home' ? 'posts.carousel-preview' : 'posts.carousel';
        elseif($this->iframe):
            $view =  'posts.iframe';
        endif;

        return $view;
    }

}
