<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


//Route::get('email', function() {
//    return new \App\Mail\LoginCredentials(\App\User::first(), '123456');
//});

//Route::get('about', 'PagesController@about')->name('pages.about');
//Route::get('archive', 'PagesController@archive')->name('pages.archive');
//Route::get('contact', 'PagesController@contact')->name('pages.contact');
//
//
//Route::get('blog/{post}', 'PostsController@show')->name('posts.show');
//Route::get('categorias/{category}', 'CategoriesController@show')->name('categories.show');
//Route::get('tags/{tag}', 'TagsController@show')->name('tags.show');

Route::auth([
    'register' => false,
    'reset' => false
]);

Route::group([
    'prefix' => 'admin',
    'namespace' => 'Admin',
    'middleware' => 'auth'], function() {

    Route::get('/', 'AdminController@index')->name('admin');

    Route::resource('posts', 'PostsController', [ "except" => "show", "as" => "admin" ]);
    Route::resource('users', 'UsersController', ['as' => 'admin']);
    Route::resource('roles', 'RolesController', ['as' => 'admin', "except" => "show"]);
    Route::resource('permissions', 'PermissionsController', ['as' => 'admin', "only" => ['index', 'edit', 'update']]);

    Route::middleware('role_or_permission:Admin|Update roles')
        ->put('users/{user}/roles', 'UsersRolesController@update')
        ->name('admin.users.roles.update');

    Route::middleware('role_or_permission:Admin|Update permissions')
        ->put('users/{user}/permissions', 'UsersPermissionsController@update')
        ->name('admin.users.permissions.update');

    Route::post('posts/{post}/photos', 'PhotosController@store')->name('admin.posts.photos.update');
    Route::delete('photos/{photo}', 'PhotosController@destroy')->name('admin.posts.photos.destroy');
});

Route::get('/{any?}', 'PagesController@spa')->name('pages.home')->where('any', '.*');
