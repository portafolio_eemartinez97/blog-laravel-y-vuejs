<?php

use App\User;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Role::truncate();
        User::truncate();
        Permission::truncate();

        $adminRole = Role::create([
            "name" => "Admin",
            "display_name" => "Administrador"
        ]);
        $writerRole = Role::create([
            "name" => "Writer",
            "display_name" => "Escritor"
        ]);

        $viewPostsPermission = Permission::create([
            "name" => 'View posts',
            "display_name" => "Ver posts"
        ]);
        $createPostsPermission = Permission::create([
            "name" => 'Create posts',
            "display_name" => "Crear posts"
        ]);
        $updatePostsPermission = Permission::create([
            "name" => 'Update posts',
            "display_name" => "Actualizar posts"
        ]);
        $deletePostsPermission = Permission::create([
            "name" => 'Delete posts',
            "display_name" => "Eliminar posts"
        ]);

        $viewPostsPermission = Permission::create([
            "name" => 'View users',
            "display_name" => "Ver usuarios"
        ]);
        $createPostsPermission = Permission::create([
            "name" => 'Create users',
            "display_name" => "Crear usuarios"
        ]);
        $updatePostsPermission = Permission::create([
            "name" => 'Update users',
            "display_name" => "Actualizar usuarios"
        ]);
        $deletePostsPermission = Permission::create([
            "name" => 'Delete users',
            "display_name" => "Eliminar usuarios"
        ]);

        $viewPermissionsPermission = Permission::create([
            "name" => 'View permissions',
            "display_name" => "Ver permisos"
        ]);
        $updatePermissionsPermission = Permission::create([
            "name" => 'Update permissions',
            "display_name" => "Actualizar permisos"
        ]);

        $viewRolesPermission = Permission::create([
            "name" => 'View roles',
            "display_name" => "Ver roles"
        ]);
        $createRolesPermission = Permission::create([
            "name" => 'Create roles',
            "display_name" => "Crear roles"
        ]);
        $updateRolesPermission = Permission::create([
            "name" => 'Update roles',
            "display_name" => "Actualizar roles"
        ]);
        $deleteRolesPermission = Permission::create([
            "name" => 'Delete roles',
            "display_name" => "Eliminar roles"
        ]);


        $admin = new User;
        $admin->name = 'Admin';
        $admin->email = 'admin@email.com';
        $admin->password = 'admin';
        $admin->save();

        $admin->assignRole($adminRole);

        $writer = new User;
        $writer->name = 'Writer';
        $writer->email = 'writer@email.com';
        $writer->password = 'writer';
        $writer->save();

        $writer->assignRole($writerRole);
    }
}
