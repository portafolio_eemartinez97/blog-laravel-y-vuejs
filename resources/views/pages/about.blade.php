@extends('layout')

@section('content')
    <section class="pages container">
        <div class="page page-about">
            <h1 class="text-capitalize">about</h1>
            <cite>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam scelerisque turpis in laoreet maximus. Curabitur quis dictum lectus. Integer bibendum sem magna, sit amet eleifend velit rhoncus non. Etiam condimentum erat a magna ultricies, vitae vehicula odio posuere. Suspendisse gravida tristique justo, sit amet molestie nunc scelerisque et. Ut porta lorem bibendum sem auctor, eu lacinia lorem vestibulum. Duis et sollicitudin elit. Cras maximus, metus sit amet iaculis iaculis, sapien quam tristique sem, in commodo nibh quam ac felis. Proin et fermentum sem. Pellentesque felis nisi, finibus et aliquam sit amet, feugiat vel mi. Proin iaculis, urna ac elementum mollis, odio orci bibendum justo, vel convallis dui tellus non mauris.
            </cite>
            <div class="divider-2" style="margin: 35px 0;"></div>
            <p>Maecenas auctor, libero sit amet varius lacinia, ante orci tincidunt lorem, at posuere neque ligula eu sapien. Etiam pellentesque, leo a porta auctor, nisi magna pellentesque mauris, eu lacinia risus lectus at diam. In vel consequat orci. Integer quis tristique enim. Proin ut orci ut ex eleifend porttitor. Vestibulum cursus sodales rhoncus.</p>
            <p>Cras ac elit non massa sodales commodo. Nullam dapibus sed orci nec finibus. Sed gravida nibh at mattis mattis. Ut bibendum orci at diam pharetra viverra. Nam pellentesque varius eleifend. Integer vitae elit viverra, mollis quam et, vehicula odio.</p>
        </div>
    </section>
@stop
