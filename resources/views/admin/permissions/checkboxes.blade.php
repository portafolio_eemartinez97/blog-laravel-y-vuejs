<div class="checkbox">
    @foreach($permissions as $id => $name)

        <label>
            <input type="checkbox"
                   value="{{ $name }}"
                   name="permissions[]"
                {{ $model->permissions->contains($id) || collect(old('permissions'))->contains($name) ? 'checked' : '' }}>
            {{ $name }}
        </label>
        <br>

    @endforeach
</div>
