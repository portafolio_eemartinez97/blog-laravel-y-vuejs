@csrf
<div class="form-group">
    <label for="identificador">Identificador:</label>

    @if($role->exists)
        <input type="text"
               id="identificador"
               value="{{ $role->name }}"
               class="form-control" disabled>
    @else
        <input type="text"
               id="identificador"
               name="name"
               value="{{ old('name', $role->name) }}"
               class="form-control">
    @endif

</div>

<div class="form-group">
    <label for="display_name">Nombre:</label>
    <input type="text"
           name="display_name"
           class="form-control"
           value="{{ old('display_name', $role->display_name) }}">
</div>

<div class="form-group">
    <label for="permissions">Permisos:</label>
    @include('admin.permissions.checkboxes', ['model' => $role])
</div>
