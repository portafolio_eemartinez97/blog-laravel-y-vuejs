<div class="checkbox">
    @foreach($roles as $role)

        <label>
            <input type="checkbox"
                   value="{{ $role->name }}"
                   name="roles[]"
                {{ $user->roles->contains($role->id) ? 'checked' : '' }}>
            {{ $role->name }} <br>
            <small class="text-muted">{{ $role->permissions->pluck('name')->implode(', ') }}</small>
        </label>
        <br>

    @endforeach
</div>
