@extends('admin.layout')

@section('content')
    <div class="row">
        <div class="col-md-6">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">
                        Datos personales
                    </h3>
                </div>
                <div class="box-body">
                    @include('partials.error-messages')
                    <form action="{{ route('admin.users.update', $user) }}" method="POST">
                        @method('PUT')
                        @csrf
                        <div class="form-group">
                            <label for="name">Nombre:</label>
                            <input type="text"
                                   name="name"
                                   value="{{ old('name', $user->name) }}"
                                   class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="email">Email:</label>
                            <input type="text"
                                   name="email"
                                   value="{{ old('email', $user->email) }}"
                                   class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="password">Contraseña:</label>
                            <input type="password"
                                   name="password"
                                   class="form-control"
                                   placeholder="Contraseña">
                            <span class="help-block">
                                Dejar en blanco si noquiered cambiar la contraseña.
                            </span>
                        </div>
                        <div class="form-group">
                            <label for="password_confirmation">Repite la contraseña</label>
                            <input type="password"
                                   name="password_confirmation"
                                   class="form-control"
                                   placeholder="Repite la Contraseña">
                        </div>
                        <button class="btn btn-primary btn-block">Actualizar Usuario</button>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <div class="box-title">Roles</div>
                </div>
                <div class="box-body">
                    @if(auth()->user()->can('Update roles') || auth()->user()->hasRole('Admin'))
                        <form action="{{ route('admin.users.roles.update', $user) }}" method="POST">
                            @method('PUT')
                            @csrf

                            @include('admin.roles.checkboxes')

                            <button class="btn btn-primary btn-block">Actualizar Roles</button>
                        </form>
                    @else
                        <ul class="list-group">
                            @forelse ($user->roles as $role)
                                <li class="list-group-item">{{ $role->name }}</li>
                            @empty
                                <li class="list-group-item">No tiene roles</li>
                            @endforelse
                        </ul>
                    @endif
                </div>
            </div>
            <div class="box box-primary">
                <div class="box-header with-border">
                    <div class="box-title">Permisos</div>
                </div>
                <div class="box-body">
                    @if(auth()->user()->can('Update roles') || auth()->user()->hasRole('Admin'))
                        <form action="{{ route('admin.users.permissions.update', $user) }}" method="POST">
                            @method('PUT')
                            @csrf

                            @include('admin.permissions.checkboxes', ['model' => $user])

                            <button class="btn btn-primary btn-block">Actualizar Permisos</button>
                        </form>
                    @else
                        <ul class="list-group">
                            @forelse($user->permissions as $permission)
                                <li class="list-group-item">{{ $permission->name }}</li>
                            @empty
                                <li class="list-group-item">No tiene permisos</li>
                            @endforelse
                        </ul>
                    @endif
                </div>
            </div>
        </div>
    </div>
@stop
