@extends('admin.layout')

@section('header')
    <h1>
        Posts
        <small>Crear publicación</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('admin') }}"><i class="fa fa-dashboard"></i> Inicio</a></li>
        <li><a href="{{ route('admin.posts.index') }}"><i class="fa fa-list"></i> Posts</a></li>
        <li class="active">Crear</li>
    </ol>
@stop

@section('content')
    <div class="row">
        @if($post->photos->count())
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-body">
                        <div class="row">
                            @foreach($post->photos as $photo)
                                <form method="POST" action="{{ route('admin.posts.photos.destroy', $photo) }}">
                                    @csrf
                                    @method('DELETE')
                                    <div class="col-md-2">
                                        <button class="btn btn-danger btn-xs" style="position: absolute;">
                                            <i class="fa fa-remove"></i>
                                        </button>
                                        <img src="{{ url($photo->path) }}" alt="" class="img-responsive">
                                    </div>
                                </form>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        @endif
        <form action="{{ route('admin.posts.update', $post) }}" method="POST">
            @csrf()
            @method('PUT')
            <div class="col-md-8">
                <div class="box box-primary">
                    <div class="box-body">

                        <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                            <label for="">Título de la publicación</label>
                            <input type="text" name="title" value="{{ old('title', $post->title) }}" class="form-control" placeholder="Ingresa aquí el título de la publicación">
                            {!! $errors->first('title', '<span class="help-block">:message</span>') !!}
                        </div>
                        <div class="form-group"{{ $errors->has('body') ? ' has-error' : '' }}>
                            <label for="">Contenido de la publicación</label>
                            <textarea id="editor" name="body" rows="10" class="form-control" placeholder="Ingresa el contenido completo de la publicación">
                                {{ old('body', $post->body) }}
                            </textarea>
                            {!! $errors->first('body', '<span class="help-block">:message</span>') !!}
                        </div>
                        <div class="form-group"{{ $errors->has('iframe') ? ' has-error' : '' }}>
                            <label for="iframe">Contenido embebido (iframe)</label>
                            <textarea id="editor" name="iframe" rows="2" class="form-control" placeholder="Ingresa Contenido embebido (iframe) de audio o video">{{ old('iframe', $post->iframe) }}</textarea>
                            {!! $errors->first('iframe', '<span class="help-block">:message</span>') !!}
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="box box-primary">
                    <div class="box-body">
                        <!-- Date -->
                        <div class="form-group">
                            <label>Fecha de publicación:</label>

                            <div class="input-group date">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <input type="text" value="{{ old('published_at', $post->published_at ? $post->published_at->format('d/m/Y') : null) }}" name="published_at" class="form-control pull-right" id="datepicker">
                            </div>
                            <!-- /.input group -->
                        </div>
                        <!-- /.form group -->
                        <div class="form-group{{ $errors->has('category_id') ? ' has-error' : '' }}">
                            <label for="">Categorias</label>
                            <select name="category_id" class="form-control select2">
                                <option value="">Selecciona una categoría</option>
                                @foreach($categories as $category)
                                    <option value="{{ $category->id }}"
                                        {{ old('category_id', $post->category_id) == $category->id ? 'selected' : '' }}
                                    >{{ $category->name }}</option>
                                @endforeach
                            </select>
                            {!! $errors->first('category_id', '<span class="help-block">:message</span>') !!}
                        </div>
                        <div class="form-group{{ $errors->has('tags') ? ' has-error' : '' }}">
                            <label for="">Etiquetas</label>
                            <select name="tags[]" class="form-control select2"
                                    multiple="multiple"
                                    data-placeholder="Selecciona una o más etiquetas"
                                    style="width: 100%;">
                                @foreach($tags as $tag)
                                    <option
                                        {{ collect(old('tags', $post->tags->pluck('id')))->contains($tag->id) ? 'selected' : '' }}
                                        value="{{ $tag->id }}">{{ $tag->name }}</option>
                                @endforeach
                            </select>
                            {!! $errors->first('tags', '<span class="help-block">:message</span>') !!}
                        </div>
                        <div class="form-group{{ $errors->has('excerpt') ? ' has-error' : '' }}">
                            <label for="">Extracto de la publicación</label>
                            <textarea name="excerpt" class="form-control" placeholder="Ingresa un extracto de la publicación.">
                                {{ old('excerpt', $post->excerpt) }}
                            </textarea>
                            {!! $errors->first('excerpt', '<span class="help-block">:message</span>') !!}
                        </div>
                        <div class="form-group">
                            <div class="dropzone"></div>
                        </div>
                        <div class="form-group">
                            <button class="btn btn-primary btn-block" type="submit">Guardar Publicación</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
@stop

@push('styles')
    <!-- Dropzone -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.5.1/min/dropzone.min.css">

    <!-- bootstrap datepicker -->
    <link rel="stylesheet" href="/adminlte/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">

    <!-- Select2 -->
    <link rel="stylesheet" href="/adminlte/bower_components/select2/dist/css/select2.min.css">
@endpush

@push('scripts')
    <!-- Dropzone -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.5.1/min/dropzone.min.js"></script>

    <!-- bootstrap datepicker -->
    <script src="/adminlte/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>

    <!-- CK Editor -->
    <script src="https://cdn.ckeditor.com/4.11.2/standard/ckeditor.js"></script>

    <!-- Select2 -->
    <script src="/adminlte/bower_components/select2/dist/js/select2.full.min.js"></script>

    <script>
        //Date picker
        $('#datepicker').datepicker({
            autoclose: true
        });
        // Replace the <textarea id="editor1"> with a CKEditor
        // instance, using default configuration.
        CKEDITOR.replace('editor');
        CKEDITOR.config.height = 315;

        //Initialize Select2 Elements
        $('.select2').select2({
            tags: true
        })

        var myDropzone = new Dropzone('.dropzone', {
            url: '/admin/posts/{{ $post->url }}/photos',
            headers: {
              'X-CSRF-TOKEN': '{{ csrf_token() }}'
            },
            acceptedFiles: 'image/*',
            maxFilesize: 2,
            paramName: 'photo',
            dictDefaultMessage: 'Arrastra las fotos aquí para subirlas'
        });

        myDropzone.on('error', function(file, res){
            var msg = res.errors.photo[0];
            $('.dz-error-message:last > span').text(msg);
        });

        Dropzone.autoDiscover = false;
    </script>
@endpush
